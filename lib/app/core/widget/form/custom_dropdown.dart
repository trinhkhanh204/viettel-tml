import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:htds_mobile/app/core/model/base_search_item.dart';


class CustomDropdown<T extends SearchItem> extends StatefulWidget {
  final String? label;
  final String formControlName;
  final Function? onChange;
  final List<T>? options;
  final FormFieldState? field;
  final List<String? Function(dynamic)> validators;
  const CustomDropdown(
      {Key? key,
      this.label,
      required this.formControlName,
        this.options,
        this.field,
        required this.validators,
      this.onChange,})
      : super(key: key);

  @override
  State<CustomDropdown> createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  @override
  Widget build(BuildContext context) {
    return FormBuilderDropdown(
      name: widget.formControlName,
      decoration: InputDecoration(
        labelText: widget.label,
        isDense: true,
        border: OutlineInputBorder(
          borderSide: BorderSide(width: 1),
          borderRadius: BorderRadius.circular(8)
        )
        // hintText: 'Select Gender'
      ),
      // autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: FormBuilderValidators.compose(widget.validators),
      items: (widget.options ?? [])
          .map((item) => DropdownMenuItem(
        alignment: AlignmentDirectional.centerStart,
        value: item.value,
        child: Text(item.label ?? ""),
      ))
          .toList(),
      onChanged: (val) {
       if (widget.onChange != null) {
         widget.onChange!(val);
       }
      },
      // valueTransformer: (val) => val?.toString(),
    );
  }
}
