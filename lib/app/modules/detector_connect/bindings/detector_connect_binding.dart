import 'package:get/get.dart';
import 'package:htds_mobile/app/modules/detector_connect/controllers/detector_connect_controller.dart';


class DetectorConnectBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetectorConnectController>(
      () => DetectorConnectController(),
    );
  }
}
