import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:htds_mobile/translations/localization_service.dart';
import 'package:local_auth/local_auth.dart';

import '../../../routes/app_pages.dart';
import '/app/core/base/base_controller.dart';

class DetectorConnectController extends BaseController {
  final formKey = GlobalKey<FormBuilderState>();
  @override
  void onInit() {
    super.onInit();
  }
}
