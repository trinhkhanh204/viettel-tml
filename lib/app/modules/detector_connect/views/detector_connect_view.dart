import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:htds_mobile/app/core/model/base_search_item.dart';
import 'package:htds_mobile/app/core/widget/form/custom_dropdown.dart';
import 'package:htds_mobile/app/core/widget/form/custom_text_field.dart';
import '../../../core/values/app_values.dart';
import '../controllers/detector_connect_controller.dart';
import '/app/core/base/base_view.dart';
import '/app/core/values/text_styles.dart';

class DetectorConnectView extends GetView<DetectorConnectController> {

  final listDevice = [
    SearchItem(label: "Thietbi1", value: '1'),
    SearchItem(label: "Thietbi2", value: '2'),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(children: [
        SizedBox(
          height: 180,
          child: Center(
            child: Image.asset('images/logo_login.png'),
          ),
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: AppValues.largePadding),
          child: Text('detectorConnect'.tr, style: extraBigTitleStyle),
        ),
        SizedBox(
          height: AppValues.largePadding,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: AppValues.largePadding),
          child: FormBuilder(
              key: controller.formKey,
              onChanged: () {
                controller.formKey.currentState!.save();
                debugPrint(controller.formKey.currentState!.value.toString());
                // controller.formKey.currentState!.validate();
              },
              // autovalidateMode: AutovalidateMode.onUserInteraction,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: AppValues.largePadding),
                    child: CustomTextField(
                      label: "Máy chủ",
                      formControlName: 'host',
                      field: controller.formKey.currentState?.fields["host"],
                      validators: [
                        FormBuilderValidators.required(
                            errorText: "Thông tin máy chủ bắt buộc nhập"),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: AppValues.largePadding),
                    child: CustomTextField(
                      label: "Cổng",
                      formControlName: 'port',
                      field: controller.formKey.currentState?.fields["port"],
                      validators: [
                        FormBuilderValidators.required(
                            errorText: "Thông tin cổng bắt buộc nhập"),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: AppValues.largePadding),
                    child:
                    FormBuilderDropdown(
                      name: "deviceName",
                      decoration: InputDecoration(
                          labelText: "Ten thiet bi",
                          isDense: true,
                          border: OutlineInputBorder(
                              borderSide: BorderSide(width: 1),
                              borderRadius: BorderRadius.circular(8)
                          )
                        // hintText: 'Select Gender'
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: FormBuilderValidators.compose([FormBuilderValidators.required()]),
                      items: listDevice
                          .map((item) => DropdownMenuItem(
                        alignment: AlignmentDirectional.centerStart,
                        value: item.value,
                        child: Text(item.label ?? ""),
                      ))
                          .toList(),
                      // valueTransformer: (val) => val?.toString(),
                    )
                  ),
                  SizedBox(height: AppValues.largePadding * 3,),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.black,
                        minimumSize: const Size.fromHeight(48),
                      ),
                      onPressed: () {
                        controller.formKey.currentState!.saveAndValidate();
                      },
                      child: Text(
                        "Dò tìm đối tượng".tr,
                        style: whiteText14,
                      )),
                  const SizedBox(
                    height: AppValues.largePadding,
                  ),
                ],
              )),
        )
      ]),
    );
  }
}
